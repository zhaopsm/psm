<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="jl" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- 用struts标签一直报错,用jstl算了 -->
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>PSM密码管理</title>
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/ebook-style.css">
<style type="text/css">
.textcolor{color: white;}
</style>
</head>
<body>	
	<div class="product-showcase">
		<div class="product-showcase-pattern">
			<div class="container">
				<div class="page-header">
					<div class="row">
						<div class="col-lg-3 col-md-offset-9">
							<i class="icon-envelope"></i>email:<a
								href="mailto:784051307@qq.com"> 784051307@qq.com</a>
						</div>
					</div>
				</div>

				<div class="col-lg-12 product-background">
						<div class="col-lg-4" style="padding: 50px">
							<img class="img-circle" src="img/2.jpg" height="100%"
								width="100%">
						</div>
						<div class="col-lg-7 col-md-offset-1">
							<h1 class="text-center textcolor">关于使用</h1>
                                <p style="padding:20px"></p>
							<p class="lead textcolor">添加网站信息时网站名作为主键不能重复,其他随意</p>
							<p class="lead textcolor">关于云端备份需注册之后您只需记住这一个密码就能轻松登陆您的其他论坛与网站.
							<p class="lead textcolor">关于本地备份以后有时间(可能会用c/s,或安卓开发暂时无法使用)
							<div style="padding: 14px" class="col-md-offset-7">
								<a type="button" class="btn btn-primary btn-lg" id="yun"
									data-toggle="modal"
									href="<jl:choose>
							<jl:when test="${sessionScope.isLogin==null?false:true}">/MyPsm2/loginaction_isLogin.action</jl:when>
							<jl:otherwise>#mymodal</jl:otherwise>
							</jl:choose>">
									云端备份 </a>
								<button type="button" class="btn btn-default btn-lg"
									disabled="disabled">本地备份</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	<div class="modal fade bs-example-modal-sm" id="mymodal" >
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">用户登陆</div>
				<form action="loginaction_isLogin.action" method="post"
					class="form-group-sm">
					<div class="modal-body">
						<div class="input-group">
							<div class="input-group-addon">@</div>
							<input class="form-control" type="email"
								placeholder="Enter email" name="loginName">
						</div>
						<p style="padding: 5px"></p>
						<div class="input-group">
							<div class="input-group-addon">
								<span class="glyphicon glyphicon-lock"></span>
							</div>
							<input class="form-control" type="password"
								placeholder="Enter password" name="loginPassWord">
						</div>
					</div>
					<div class="modal-footer">
						<button class=" btn btn-primary" type="submit">登陆</button>
						<button class="btn btn-default" data-dismiss="modal">返回</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div style="padding: 10px 40px"><p class="lead">测试账号上面邮箱,密码admin.本站仅供学习,欢迎有兴趣有问题有建议的小伙伴跟我联系.另外好像不兼容ie,推荐使用其他浏览器</p></div>
	<script src="js/jquery-2.1.1.min.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="js/jquery.backstretch.min.js"></script>
	<script src="js/ebook-scripts.js"></script>
</body>
</html>