jQuery(document).ready(function() {
    $('.product-showcase').backstretch('img/1.jpg');
    $('.gallery-images .img-wrapper').hover(
        function() {
            $(this).find('.img-background').fadeIn('fast');
        },
        function() {
            $(this).find('.img-background').fadeOut('fast');
        }
    );
    $(".gallery-images a[rel^='prettyPhoto']").prettyPhoto({social_tools: false});
    $(".show-tweet").tweet({
        join_text: "auto",
        count: 1,
        loading_text: "loading tweet...",
        template: "{text} {time}"
    });
});


