<%@ page language="java" import="java.util.*,com.psm.pojo.*"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>PSM密码管理</title>
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/ebook-style.css">
<%
	List<Datauser> datalist = (List) session
			.getAttribute("datauserlist");
			List<String> message =(List<String>)session.getAttribute("message");
%>
<script type="text/javascript">
	function copy(obj) {
		if (window.clipboardData) {
			window.clipboardData.clearData();
			window.clipboardData.setData("Text", obj.value);
			alert("复制成功！");
		} else if (navigator.userAgent.indexOf("Opera") != -1) {
			window.location = obj.value;
			alert("复制成功！");
		}else
		alert("您目前浏览器不能进行复制");
	}
	function ps(p) {
			document.getElementById(p.id).innerHTML = "<input type=\"text\" id="+p.id+" class=\"form-control\" onclick=\"javascript:te(this)\"  style=\"opacity: 0.4\" value="
					+ p.value + ">";

	}
	function te(p) {
			document.getElementById(p.id).innerHTML = "<input type=\"password\" id="+p.id+" class=\"form-control\" readonly=\"readonly\" onclick=\"javascript:ps(this)\" style=\"opacity: 0.4\" value="
					+ p.value + ">";

	}
</script>
</head>
<body>
	<div class="product-showcase">
		<div class="product-showcase-pattern">
			<div class="container">
			<div class="page-header">
			<div class="row">
				<div class="col-lg-3 col-md-offset-9">
					<i class="icon-envelope"></i>email:<a
						href="mailto:784051307@qq.com"> 784051307@qq.com</a>
				</div>
			</div>
		</div>
	
				<div class="col-lg-12 product-background">
					<div style="padding: 10px">
						<ul class="nav nav-pills">
							<li class="active"><a href="#">首页</a>
							</li>
							<li><a href="#">批处理</a>
							</li>
							<li><a href="About.html">关于</a>
							</li>
							<li><a href="#">留言板</a>
							</li>
							<li><a href="#">MyBlog</a>
							</li>
							<li class="active"><a href="#mymodal"  data-toggle="modal">增加</a></li>
						</ul>
					</div>
					<div class="col-xs-9">
						<table class="table table-hover" name="mytable" style="color:white;">
							<tr>
								<td align="center">网站名字</td>
								<td align="center">账号</td>
								<td align="center">密码</td>
								<td align="center">设置</td>
							</tr>
							
								<%
									for (Datauser datauser : datalist) {
								%>
								<tr>
								<td align="center" class="lead"><a href="<%=datauser.getUrl()%>"><%=datauser.getWebname()%></a>
								</td>
								<td align="center" onclick="copy(this)"><%=datauser.getName()%></td>
								<td align="center">
									<form action="" name="forms">
										<div class="input-group col-lg-7">
											<span id="<%=datauser.getWebname()%>"> <input type="password" id="<%=datauser.getWebname()%>"
												readonly="readonly" 
												onclick="javascript:ps(this)" class="form-control"
												style="opacity: 0.4" value="<%=datauser.getPassword()%>">
											</span>
										</div>
									</form>
								</td>
								<td align="center"><button class="btn" disabled="disabled">修改</button>
								<a class="btn" href="deldata.action?webName=<%=datauser.getWebname()%>">删除</a></td>
								</tr>
								<%
									}
								%>
							
						</table>
						<strong>共<strong style="color: red;"><s:property value="pagebean.allRow"/></strong>条数据    当前第<strong style="color: red;"><s:property value="pagebean.currentPage"/></strong>页</strong>
						<div align="right">
						<ul class="pagination pagination-lg">
							<li><a
									href="loginaction_showMain.action?page=
									<s:if test="pagebean.currentPage==1">
																		<s:property value="pagebean.currentPage"/>
									
									</s:if>
									<s:else>									<s:property value="pagebean.currentPage-1"/>
									</s:else>
									">&laquo;</a>
								</li>
							<li><a href="loginaction_showMain.action?page=1">1</a>
							</li>
							<li><a href="loginaction_showMain.action?page=2">2</a>
							</li>
							<li><a href="loginaction_showMain.action?page=3">3</a>
							</li>
							<li><a href="loginaction_showMain.action?page=4">4</a>
							</li>
							<li><a href="loginaction_showMain.action?page=<s:property value="pagebean.totalPage"/>"><s:property value="pagebean.totalPage"/></a>
							</li>
								<li><a
									href="loginaction_showMain.action?page=
									<s:if test="pagebean.currentPage==pagebean.totalPage">
																		<s:property value="pagebean.currentPage"/>
									
									</s:if>
									<s:else>									<s:property value="pagebean.currentPage+1"/>
									</s:else>
									">&raquo;</a>
								</li>
							</ul>
					</div>
					</div>

					<div class="col-xs-3" align="right" style="color: white;">
						<div class="col3" >
							<%
								for (String m : message) {
							%>
							<p class="lead"><%=m%>
								<%} %>
								
						</div>
						<div align="right" style="padding:330px 100px"><p class="small">这是个可爱的留言板</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade " id="mymodal" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					添加新账户
					<s:property value="#session.id" />
				</div>
				<form action="adddata.action" class="form-horizontal" role="form"
					method="post">

					<div class="modal-body">
						<div class="form-group">
							<label for="webname" class="col-sm-2 control-label">站名</label>

							<div>
								<input id="webname" type="text" name="webname"  required>
							</div>
						</div>
						<div class="form-group">
							<label for="weburl" class="col-sm-2 control-label">网址</label>

							<div>
								<input id="weburl" type="text" name="url" value="http://" required>
							</div>
						</div>
						<div class="form-group">
							<label for="dataname" class="col-sm-2 control-label">账号</label>

							<div>
								<input id="dataname" type="email" name="name" placeholder="Enter email" required>
							</div>
						</div>
						<div class="form-group">

							<label for="datapassword" class="col-sm-2 control-label">密码</label>

							<div class="inner-addon left-addon">
								<input id="datapassword" type="text" name="password"
									placeholder="password" required>
							</div>
						</div>

					</div>
					<div class="modal-footer">
						<button class="btn btn-default" data-dismiss="modal">返回</button>
						<button type="submit" class="btn btn-primary">确认添加</button>
					</div>

				</form>
			</div>
		</div>
	</div>
	<script src="js/jquery-2.1.1.min.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="js/jquery.backstretch.min.js"></script>
	<script src="js/ebook-scripts.js"></script>
</body>
</html>