package com.psm.action;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionSupport;
import com.psm.dao.impl.DelDataDaoImpl;

@Controller@Scope("prototype")
public class DelUserDataAction extends ActionSupport {
	private static final long serialVersionUID = 1L;
	@Resource DelDataDaoImpl delDataDao;
	private String webName;
 	public String getWebName() {
		return webName;
	}

	public void setWebName(String webName) {
		this.webName = webName;
	}
	public String execute() throws Exception {
		webName = new String(webName.getBytes("iso-8859-1"), "utf-8");
		delDataDao.delDataByWebName(webName);
		return SUCCESS;
	}

}
