package com.psm.action;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.psm.dao.impl.AddDataDaoImpl;
import com.psm.pojo.Datauser;
@Controller@Scope("prototype")
public class AddUserDataAction extends ActionSupport implements
		ModelDriven<Datauser> {
	private static final long serialVersionUID = 1L;
	Datauser datauser = new Datauser();
@Resource AddDataDaoImpl addDataDao;
	public Datauser getModel() {
		return datauser;
	}
	public Datauser getDatauser() {
		return datauser;
	}
	public void setDatauser(Datauser datauser) {
		this.datauser = datauser;
	}
	public String execute() throws Exception {
		datauser.setId((Integer) ActionContext.getContext().getSession()
				.get("id"));//不然id为空了
		addDataDao.addData(datauser);
		return SUCCESS;
	}
}
