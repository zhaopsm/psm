package com.psm.action;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.psm.Services.PageUtilService;
import com.psm.beans.PageBean;
import com.psm.dao.impl.LoginDaoImpl;
import com.psm.pojo.Loginuser;

@Controller
@Scope("prototype")
public class LoginAction extends ActionSupport implements
		ModelDriven<Loginuser> {
	private static final long serialVersionUID = 1L;
	private Loginuser user = new Loginuser();
	private int page = 1;// 默认 第几页,不能写0不然在第一页再上一页就为负了.
	private PageBean pagebean;// 包含分页信息集合的bean
	@Resource
	PageUtilService pageServiceimpl;// 事实证明不是接口也是可以注入的
	@Resource
	LoginDaoImpl logindaoimpl;

	// 为什么上面的换个位置就可以了
	public Loginuser getModel() {
		return user;
	}

	public Loginuser getUser() {
		return user;
	}

	public void setUser(Loginuser user) {
		this.user = user;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public PageBean getPagebean() {
		return pagebean;
	}

	public void setPagebean(PageBean pagebean) {
		this.pagebean = pagebean;
	}

	// 登陆方法
	public String isLogin() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("isLogin") != null
				&& session.getAttribute("isLogin").equals("yes"))
			return showMain();// 为什么开始用==好像可以的样子还非得用equals
		else if (logindaoimpl.login(user) == true) {
			session.setAttribute("isLogin", "yes");
			List<String> list = new ArrayList<String>();
			list.add("欢迎"+user.getLoginName()+"访问本站");
			session.setAttribute("message", list);
			return showMain();
		} else
			return ERROR;
	}
	// 主页分页显示方法
	public String showMain() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		this.pagebean = pageServiceimpl.queryForPage(10, page,
				(Integer) session.getAttribute("id"));// put和getattribute居然可以共用
		session.setAttribute("datauserlist", this.pagebean.getList());
		// 这里必须要用setAttribute,但是上面可以直接用ActionContext得到的来put,因为不是对象
		return "main";
	}
}
