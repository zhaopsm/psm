package com.psm.action;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.psm.dao.impl.RegDaoImpl;
import com.psm.pojo.Loginuser;
@Controller@Scope("prototype")
public class reg extends ActionSupport implements ModelDriven<Loginuser> {
	private static final long serialVersionUID = 1L;
	private Loginuser loginuser=new Loginuser();
@Resource RegDaoImpl RegDao;
	public Loginuser getLoginuser() {
		return loginuser;
	}

	public void setLoginuser(Loginuser loginuser) {
		this.loginuser = loginuser;
	}

	public String execute() throws Exception {
		RegDao.addLoginUser(loginuser);
		return SUCCESS;
	}

	public Loginuser getModel() {
		return loginuser;
	}

}
