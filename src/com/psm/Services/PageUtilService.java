package com.psm.Services;

import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.psm.beans.PageBean;
import com.psm.dao.impl.PageUtilImpl;
import com.psm.pojo.Datauser;

@Service
public class PageUtilService{
	@Resource PageUtilImpl pagedao;
	public PageBean queryForPage(int pageSize, int currentPage,int id) {
		final String hql = "from Datauser where id ="+id;
		int allRow = pagedao.getAllRowCount(hql);// 查询得到总的数据,肯定要经过数据库不然怎么查到so用dao来操作
		int totalPage = PageBean.countTotalPage(pageSize, allRow);// 得到总的页数
		int offset = PageBean.countOffset(pageSize, currentPage);// 当前的页的数据第一个数据的数据号
		List<Datauser> list = pagedao.queryForPage(hql, offset, pageSize);
		PageBean pageBean = new PageBean();
		pageBean.setPageSize(pageSize);
		pageBean.setCurrentPage(currentPage);
		pageBean.setAllRow(allRow);
		pageBean.setTotalPage(totalPage);
		pageBean.setList(list);
		pageBean.init();
		return pageBean;
	}

}
