package com.psm.pojo;
public class Loginuser implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String loginName;
	private String loginPassWord;

	
	public Loginuser() {
	}

	/** full constructor */
	public Loginuser(String loginName, String loginPassWord) {
		this.loginName = loginName;
		this.loginPassWord = loginPassWord;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLoginName() {
		return this.loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getLoginPassWord() {
		return this.loginPassWord;
	}

	public void setLoginPassWord(String loginPassWord) {
		this.loginPassWord = loginPassWord;
	}

}