package com.psm.pojo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Datauser implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Integer id;
	private String url;
	private String webname;
	private String name;
	private String password;
	public Datauser() {
	}

	public Datauser(Integer id, String name, String password) {
		this.id = id;
		this.name = name;
		this.password = password;
	}

	public Datauser(Integer id, String url, String webname, String name,
			String password) {
		this.id = id;
		this.url = url;
		this.webname = webname;
		this.name = name;
		this.password = password;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getWebname() {
		return this.webname;
	}

	public void setWebname(String webname) {
		this.webname = webname;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}