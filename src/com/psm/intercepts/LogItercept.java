package com.psm.intercepts;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

public class LogItercept extends AbstractInterceptor {
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	@Override
	public String intercept(ActionInvocation arg0) throws Exception {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> m = arg0.getInvocationContext().getParameters();
		String nowtime = new SimpleDateFormat("HH:mm:ss").format(System
				.currentTimeMillis());
		List<String> str;
		if (session.getAttribute("message") == null) {
			str = new ArrayList<String>();
		} else {
			str = (List<String>) session.getAttribute("message");
		}
		if (arg0.getInvocationContext().getName().equals("deldata")) {
			String[] s = (String[]) m.get("webName");
			String del = new String(s[0].getBytes("iso-8859-1"), "utf-8");
			str.add(nowtime + "你刚刚删除了一条关于" + del + "的账号信息");
		} else if (arg0.getInvocationContext().getName().equals("adddata")) {
			String[] s = (String[]) m.get("webname");// 尽管传的是对象,用的是模型但是只要把name找准了还是对的上的.注意和pojo一样
			str.add(nowtime + "你刚刚增加了一条关于" + s[0] + "的账号信息");
		}
		session.setAttribute("message", str);
		return arg0.invoke();
	}
}
