package com.psm.dao.impl;

import com.psm.pojo.Loginuser;

public interface LoginDaoImpl {
	public boolean login(Loginuser loginuser);
}
