package com.psm.dao.impl;

import java.util.List;

import com.psm.pojo.Datauser;

public interface PageUtilImpl {
	public List<Datauser> queryForPage(final String hql,final int offset,final int length);
	public int getAllRowCount(String hql);
}
