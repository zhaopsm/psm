package com.psm.dao;

import javax.annotation.Resource;

import org.hibernate.*;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.psm.dao.impl.DelDataDaoImpl;
import com.psm.pojo.Datauser;

@Transactional
@Repository
public class DelDataDao implements DelDataDaoImpl {
	@Resource
	SessionFactory sessionFactory;

	public void delDataByWebName(String webName) {
		Session session = sessionFactory.openSession();
		Transaction tr = session.beginTransaction();
		session.delete(session.get(Datauser.class, webName));
		tr.commit();
		session.close();
	}
}
