package com.psm.dao;

import javax.annotation.Resource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.psm.dao.impl.AddDataDaoImpl;
import com.psm.pojo.Datauser;

@Transactional
@Repository
public class AddDataDao implements AddDataDaoImpl {
	@Resource
	private SessionFactory sessionFactory;

	public void addData(Datauser datauser) {
		Session session = sessionFactory.openSession();
		Transaction tr = session.beginTransaction();
		session.save(datauser);
		tr.commit();
		session.close();
	}

}
