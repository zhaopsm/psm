package com.psm.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.opensymphony.xwork2.ActionContext;
import com.psm.dao.impl.LoginDaoImpl;
import com.psm.pojo.Loginuser;

@Transactional
@Repository
public class LoginDao implements LoginDaoImpl {
	@Resource
	private SessionFactory sessionFactory;

	public boolean login(Loginuser loginuser) {
		boolean islogin = false;
		Session session = sessionFactory.openSession();
		String LoginHQL = "from Loginuser where  LoginName = ? and LoginPassWord = ?";
		@SuppressWarnings("unchecked")
		List<Loginuser> loginuserlist = session.createQuery(LoginHQL)
				.setString(0, loginuser.getLoginName())
				.setString(1, loginuser.getLoginPassWord()).list();
		if (loginuserlist.size() != 0) {
			ActionContext.getContext().getSession()
					.put("id", loginuserlist.get(0).getId());
			islogin = true;
		}
		return islogin;
	}
}
