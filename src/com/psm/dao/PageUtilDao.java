package com.psm.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.psm.dao.impl.PageUtilImpl;
import com.psm.pojo.Datauser;

@Transactional
@Repository
public class PageUtilDao implements PageUtilImpl {
	@Resource
	private SessionFactory sessionFactory;

	// 总数据量
	public int getAllRowCount(String hql) {
		Session session = sessionFactory.openSession();
		int size = session.createQuery(hql).list().size();
		session.close();
		return size;
	}
	public List<Datauser> queryForPage(final String hql, final int offset,
			final int length) {
		Session session = sessionFactory.openSession();
		Query query = session.createQuery(hql);
		query.setFirstResult(offset);
		query.setMaxResults(length);
		@SuppressWarnings("unchecked")
		List<Datauser> list = query.list();
		session.close();
		return list;
	}
}
