package com.psm.dao;

import javax.annotation.Resource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.psm.dao.impl.RegDaoImpl;
import com.psm.pojo.Loginuser;
@Transactional
@Repository
public class RegDao implements RegDaoImpl {
@Resource SessionFactory sessionFactory;
	public void addLoginUser(Loginuser loginuser) {
		Session session =sessionFactory.openSession();
		Transaction tr =session.beginTransaction();
		session.save(loginuser);
		tr.commit();
		session.close();
	}

}
