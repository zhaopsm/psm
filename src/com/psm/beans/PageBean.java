package com.psm.beans;

import java.util.List;

import com.psm.pojo.Datauser;

public class PageBean {
	private List<Datauser> list;
	private int allRow;// 总的数据，而不是页数
	private int totalPage;// 总页数
	private int currentPage;// 当前页
	private int pageSize;// 可以用allRow/totalPage得到
	@SuppressWarnings("unused")
	private boolean isFirstPage;
	@SuppressWarnings("unused")
	private boolean isLastPage;
	@SuppressWarnings("unused")
	private boolean hasPrePage;
	@SuppressWarnings("unused")
	private boolean hasNextPage;

	public boolean isFirstPage() {
		return currentPage == 1;
	}

	public boolean isLastPage() {
		return currentPage == totalPage;
	}

	public boolean isHasPrePage() {
		return currentPage != 1;
	}

	public boolean isHasNextPage() {
		return currentPage != totalPage;
	}

	public void init() {
		this.isFirstPage = isFirstPage();
		this.isLastPage = isLastPage();
		this.hasPrePage = isHasPrePage();
		this.hasNextPage = isHasNextPage();
	}

	/* 得到一共有多少页, */
	public static int countTotalPage(int pageSize, int allRow) {

		int totalPage = allRow % pageSize == 0 ? allRow / pageSize : allRow
				/ pageSize + 1;
		return totalPage;

	}

	// 当前页是从哪个数据开始计起(也就是当前第一个数据号,currentPage当前页数号,注意和数据号分开).
	public static int countOffset(int pageSize, int currentPage) {
		int offset = pageSize * (currentPage - 1);
		return offset;
	}

	// 如果当前页为0或者没请求的page数据,用1代替.就是默认
	public static int countCurrentPage(int page) {
		int curPage = (page == 0 ? 1 : page);
		return curPage;
	}

	public List<Datauser> getList() {
		return list;
	}

	public void setList(List<Datauser> list) {
		this.list = list;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setFirstPage(boolean isFirstPage) {
		this.isFirstPage = isFirstPage;
	}

	public void setLastPage(boolean isLastPage) {
		this.isLastPage = isLastPage;
	}

	public void setHasPrePage(boolean hasPrePage) {
		this.hasPrePage = hasPrePage;
	}

	public void setHasNextPage(boolean hasNextPage) {
		this.hasNextPage = hasNextPage;
	}

	public int getAllRow() {
		return allRow;
	}

	public void setAllRow(int allRow) {
		this.allRow = allRow;
	}

}